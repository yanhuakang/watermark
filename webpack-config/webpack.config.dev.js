const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './test/index.js',

  devServer: {
    client: {
      logging: 'none',
      overlay: false,
    },
    open: true,
    devMiddleware: { // 一个开发环境的中间件
      index: true,
      // writeToDisk: true, // 写入硬盘（library模式下自动创建app dist 岂不美哉）
    },
  },

  devtool: 'eval-source-map',

  plugins: [
    new HtmlWebpackPlugin({
      template: './test/index.html',
    }),
  ],

  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
    ],
  },
};
