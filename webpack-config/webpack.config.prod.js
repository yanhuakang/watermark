const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/watermark.js',
  target: ['web', 'es5'],
  experiments: {
    outputModule: true,
  },
  output: {
    path: path.resolve(__dirname, '../dist/es'),
    filename: 'watermark.js',
    library: {
      type: 'module',
    },
  },
};
