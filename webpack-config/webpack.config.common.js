module.exports = {
  output: {
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/, // 排除编译 node_modules
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    useBuiltIns: 'usage',
                    corejs: 3, // 添加corejs配置
                  },
                ],
              ],
              plugins: ['@babel/plugin-transform-runtime'],
            },
          },
          {
            loader: 'eslint-loader',
          },
        ],
      },
    ],
  },
};
