// import Watermark from '../src/watermark'
import Watermark from '../dist/es/watermark';
import img from './images/lb.jpeg';
import my from './images/my.svg';

const watermark = new Watermark({
  content: '张三三[123456789] 2022-05-19 16:14',
  tip: '支持双列文本水印，支持更多场景需求',
  // fontWeight: 700,
  fontSize: 12,
  fontFamily: 'microsoft yahei,Tahoma,"\\5B8B\\4F53",Arial,Helvetica,sans-serif',
  // color: 'red',
  alpha: 0.15,
  // width: 200,
  height: 200,
  // rotate: 330,
  // zIndex: '2147483647',
  onSuccess() {
    console.log('onSuccess');
  },
  onWatermarkNull: () => {
    document.body.remove();
    setTimeout(() => {
      alert('页面水印不存在，请联系管理员！');
    });
  },
});
watermark.create();

// setTimeout(() => {
//   watermark.destroy();
// }, 2000);
//
// setTimeout(() => {
//   watermark.create();
// }, 4000);

const watermark2 = new Watermark({
  // content: '勋厚系统运营[100000763] 2022-03-29 17:01',
  // image: 'https://gw.alipayobjects.com/zos/bmw-prod/59a18171-ae17-4fc5-93a0-2645f64a3aca.svg',
  image: my,
  imageWidth: 115,
  imageHeight: 36,
  // fontWeight: 400,
  fontSize: 12,
  // fontFamily: 'microsoft yahei,Tahoma,"\\5B8B\\4F53",Arial,Helvetica,sans-serif',
  // color: 'red',
  alpha: 0.15,
  // width: 300,
  // height: '190',
  rotate: 330,
  // zIndex: '2147483647',
  onSuccess() {
    console.log('onSuccess');
  },
  onWatermarkNull: () => {
    document.body.remove();
    setTimeout(() => {
      alert('页面水印不存在，请联系管理员！');
    });
  },
});
watermark2.create();

// const imgDom = document.createElement('img');
// imgDom.src = img;
// document.body.appendChild(imgDom);
