# js 水印插件

> - 支持文本水印
> - 支持双列文本水印
> - 支持图片水印
> - 可防止被删除
> - 可防止重复渲染
> - 可动态适应超长水印文本
> - 高清晰度
> - 支持浏览器端打印
> - 可配置性强



## 效果图片

### 文本水印

![文本水印](https://img-blog.csdnimg.cn/a1dd19895cf5472e9981fce93bdeca30.png)



<br />



### 双列水印文本

![双列水印文本](https://img-blog.csdnimg.cn/6e6c9126b8c048f3a0c81575efb7d163.png)



<br />



### 图片水印

通过图片水印方式可以实现`私人订制`，步骤：

> ①通过HTML + CSS 实现你想要的内容和样式；
> ②通过 html2canvas 类似操作将dom转canvas再转image；
> ③将image传入水印插件

![图片水印](https://img-blog.csdnimg.cn/3e0eaa1970454887859db4bd73fca6eb.png)



<br />



### 可防止被手动删除

![可防止被手动删除](https://img-blog.csdnimg.cn/c22d6a6af2e04f11bdf09b6c75f241eb.gif)



<br />



### 项目中效果

![项目中效果](https://img-blog.csdnimg.cn/083ca9ed5f1f45e2bc00e10c265b4a0d.gif)



<br />



## 使用

```js
import Watermark from 'watermark-plus';

const watermark = new Watermark({
  // 传参
  content: 'Hello World!',
});

// 创建水印
watermark.create();
```

或者

```js
const { default: Watermark } = require('watermark-plus');

const watermark = new Watermark({
  // 传参
  image: 'https://gw.alipayobjects.com/zos/bmw-prod/59a18171-ae17-4fc5-93a0-2645f64a3aca.svg',
});

// 创建水印
watermark.create();
```

<br />



## API

### props

| 属性            | 说明                                                        | 默认值                                                       | 类型                  | 版本  |
| --------------- | ----------------------------------------------------------- | ------------------------------------------------------------ | --------------------- | ----- |
| content         | 文本水印【**`与image必填其一`**】                           |                                                              | String                |       |
| image           | 图片水印【**`与content必填其一`**】                         |                                                              | Image\|URL\|base64    | 1.5.0 |
| tip             | 第二列文本水印文本，content 必填                            | -                                                            | String                | 1.6.0 |
| imageWidth      | 水印图片宽度                                                | 120                                                          | String\|Number        | 1.5.0 |
| imageHeight     | 水印图片高度                                                | 64                                                           | String\|Number        | 1.5.0 |
| fontWeight      | font-weight                                                 | normal                                                       | String\|Number        |       |
| fontSize        | font-size 单位px                                            | 14                                                           | String\|Number        |       |
| fontFamily      | font-family                                                 | sans-serif                                                   | String                |       |
| color           | 水印文本颜色                                                | #666666                                                      | *color*\|*hex*\|*rgb* |       |
| alpha           | 水印文本透明度 0~1 <br />0: 表示完全透明，1: 表示完全不透明 | 0.15                                                         | String\|Number        |       |
| width           | 单个水印宽度 单位px                                         | 200                                                          | String\|Number        |       |
| height          | 单个水印高度 单位px                                         | 170                                                          | String\|Number        |       |
| maxWidth        | 单个水印最大宽度 单位px                                     | 380                                                          | String\|Number        | 1.4.0 |
| maxHeight       | 单个水印最大高度 单位px                                     | 260                                                          | String\|Number        | 1.4.0 |
| rotate          | 水印旋转角度，每个水印以文本中心为原点旋转                  | 330                                                          | String\|Number        |       |
| zIndex          | z-index                                                     | 2147483647                                                   | String\|Number        |       |
| onSuccess       | 当水印创建成功的回调                                        | 创建成功后，<br />`window.HSALPWATERMARK = true`<br />表示已创建水印，以防止重复创建水印 | Function              |       |
| onWatermarkNull | 当水印被破坏消失后的回调                                    | alert                                                        | Function              |       |

> **注意：**
>
> 插件内部根据传入的参数动态计算水印文本显示完整时最小宽高，当超出设置的 width、height 时，会动态赋值width、height，但最大不超过maxWidth、maxHeight



<br />



### 方法

| 方法    | 说明     | 版本  |
| ------- | -------- | ----- |
| create  | 创建水印 |       |
| destroy | 销毁水印 | 1.4.0 |



## 兼容性

![兼容性](https://img-blog.csdnimg.cn/d895a29298f441c3997bdd0ac8b4541a.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5LqU6JmO5oiY55S75oif,size_20,color_FFFFFF,t_70,g_se,x_16)
