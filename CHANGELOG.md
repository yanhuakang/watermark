## <small>1.6.1 (2023-06-26)</small>

* perf: 优化代码 ([5a108e4](https://gitee.com/yanhuakang/watermark/commits/5a108e4))
* feat: 水印宽高修改为100% ([a6800fb](https://gitee.com/yanhuakang/watermark/commits/a6800fb))



## 1.6.0 (2022-10-20)

* feat: 支持双列文本水印 ([deae496](https://gitee.com/yanhuakang/watermark/commits/deae496))



## <small>1.5.5 (2022-07-10)</small>




## <small>1.5.4 (2022-05-21)</small>

* perf: 重新创建水印时取已有水印图片 ([68c73c0](https://gitee.com/yanhuakang/watermark/commits/68c73c0))



## <small>1.5.3 (2022-05-19)</small>




## <small>1.5.2 (2022-05-19)</small>




## <small>1.5.1 (2022-04-26)</small>

* feat: 修改文档 ([6dae447](https://gitee.com/yanhuakang/watermark/commits/6dae447))



## 1.5.0 (2022-04-26)

* feat: 1. watermarkText => content ([05b5cfc](https://gitee.com/yanhuakang/watermark/commits/05b5cfc))
* feat: A plugin that prevents watermarks from being deleted ([4614219](https://gitee.com/yanhuakang/watermark/commits/4614219))
* feat: build ([74d0e8a](https://gitee.com/yanhuakang/watermark/commits/74d0e8a))
* feat: build ([d5985b6](https://gitee.com/yanhuakang/watermark/commits/d5985b6))
* feat: build ([a74b992](https://gitee.com/yanhuakang/watermark/commits/a74b992))
* feat: build ([8bb9cdd](https://gitee.com/yanhuakang/watermark/commits/8bb9cdd))
* feat: homepage ([b203379](https://gitee.com/yanhuakang/watermark/commits/b203379))
* feat: init ([0aad3a8](https://gitee.com/yanhuakang/watermark/commits/0aad3a8))
* feat: 优化onWatermarkNull ([bcfdd60](https://gitee.com/yanhuakang/watermark/commits/bcfdd60))
* feat: 优化start ([6913f23](https://gitee.com/yanhuakang/watermark/commits/6913f23))
* feat: 优化文档 ([61f815a](https://gitee.com/yanhuakang/watermark/commits/61f815a))
* feat: 修改配置 ([3a7ddee](https://gitee.com/yanhuakang/watermark/commits/3a7ddee))
* feat: 入参 ([e837401](https://gitee.com/yanhuakang/watermark/commits/e837401))
* feat: 删除冗余代码 ([46790d0](https://gitee.com/yanhuakang/watermark/commits/46790d0))
* feat: 新增：水印文本超长，动态计算宽高密度； ([0c6410c](https://gitee.com/yanhuakang/watermark/commits/0c6410c))
* feat: 新增图片水印 ([17dc388](https://gitee.com/yanhuakang/watermark/commits/17dc388))
* feat: 水印高清晰度 ([a136633](https://gitee.com/yanhuakang/watermark/commits/a136633))
* feat: 测试 ([d85d707](https://gitee.com/yanhuakang/watermark/commits/d85d707))
* feat: 测试 ([24d192e](https://gitee.com/yanhuakang/watermark/commits/24d192e))
* feat: 测试 ([eaeaf87](https://gitee.com/yanhuakang/watermark/commits/eaeaf87))
* feat: 防止重复创建水印 ([fa0cacf](https://gitee.com/yanhuakang/watermark/commits/fa0cacf))



